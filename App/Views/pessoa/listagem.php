
<!DOCTYPE html>
<html>
    <?php include '../header/header.php'; ?>
    <body>

        <div class="container-fluid">
            <div class="row">
            
                <?php include '../menu/menu.php'; ?>

                <!-- Conteúdo Principal -->
                <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 corpo-principal" >
                    <h1>LISTAGEM DE PESSOAS</h1>  
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>CPF</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>João da Silva</td>
                                <td>123.456.789-00</td>
                            </tr>
                        </tbody>
                    </table>

                </main>
            </div>
        </div>

    </body>
    
</html>