
<!DOCTYPE html>
<html>
    <?php include '../header/header.php'; ?>
    <body>

        <div class="container-fluid">
            <div class="row">
            
                <?php include '../menu/menu.php'; ?>

                <!-- Conteúdo Principal -->
                <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 corpo-principal" >
                    <h1>CADASTRAR PESSOA</h1>

                    <div class="formulario-pessoa" >
                        <form id="cpfForm">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" id="nome" name="nome" required>
                            </div>
                            <div class="form-group">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" id="cpf" name="cpf" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </form>
                    </div>
                </main>
            </div>
        </div>

    </body>

    <script>
        $(document).ready(function() {
            $('#cpf').inputmask('999.999.999-99');

            // Função para validar CPF
            function validaCPF(cpf) {
                cpf = cpf.replace(/[\.-]/g, ''); // Remove pontos e traços do CPF
                if (cpf.length !== 11) 
                    return false; // O CPF deve ter 11 dígitos
                
                // Validação do primeiro dígito verificador
                var soma = 0;
                for (var i = 0; i < 9; i++) {
                    soma += parseInt(cpf.charAt(i)) * (10 - i);
                }
                var resto = (soma * 10) % 11;
                if (resto === 10 || resto === 11) 
                    resto = 0;
                
                if (resto !== parseInt(cpf.charAt(9))) 
                    return false; // CPF inválido
                
                // Validação do segundo dígito verificador
                soma = 0;
                for (var i = 0; i < 10; i++) {
                    soma += parseInt(cpf.charAt(i)) * (11 - i);
                }
                resto = (soma * 10) % 11;
                if (resto === 10 || resto === 11) 
                    resto = 0;
                
                if (resto !== parseInt(cpf.charAt(10))) {
                    return false; // CPF inválido
                }
                
                return true; // CPF válido
            }

            // Tratar o envio do formulário
            $('#cpfForm').submit(function(event) {
                event.preventDefault();
                var nome = $('#nome').val();
                var cpf = $('#cpf').val();

                // Validar o CPF
                if (validaCPF(cpf)) 
                    alert('CPF válido! Nome: ' + nome + ', CPF: ' + cpf);
                else 
                    alert('CPF inválido! Por favor, verifique o CPF.');
                
            });
        });
    </script>


</html>
