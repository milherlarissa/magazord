
<!DOCTYPE html>
<html>
    <?php include '../header/header.php'; ?>
    <body>

        <div class="container-fluid">
            <div class="row">
            
                <?php include '../menu/menu.php'; ?>

                <!-- Conteúdo Principal -->
                <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 corpo-principal" >
                    <h1>CADASTRAR CONTATO</h1>

                    <div class="formulario-contato" >
                        <form action="processar_formulario.php" method="post">
                            <div class="form-group">
                                <label for="pessoa">Pessoa:</label>
                                <select name="pessoa" id="pessoa" class="form-control">
                                    <option value="cliente">Cliente</option>
                                    <option value="fornecedor">Fornecedor</option>
                                    <option value="funcionario">Funcionário</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tipo">Tipo:</label>
                                <select name="tipo" id="tipo" class="form-control">
                                    <option value="email">Email</option>
                                    <option value="telefone">Telefone</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="descricao">Descrição:</label>
                                <input type="text" name="descricao" id="descricao" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </form>
                    </div>
                </main>
            </div>
        </div>

    </body>

    <script>
    
    </script>


</html>
