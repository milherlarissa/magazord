
<!DOCTYPE html>
<html>
    <?php include 'header/header.php'; ?>
    <body>

        <div class="container-fluid">
            <div class="row">
            
                <?php include 'menu/menu.php'; ?>

                <!-- Conteúdo Principal -->
                <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 corpo-principal" >
                    <h1>CONTROLE DE PESSOAS E CONTRATOS</h1>

                    <div class="botoes" >
                        <a href="./pessoa/cadastrar.php">
                            <button class="btn btn-primary">CADASTRAR PESSOA</button>
                        </a>

                        <a href="./contato/cadastrar.php">
                            <button class="btn btn-primary">CADASTRAR CONTATO</button>
                        </a>

                    </div>

                </main>

            </div>
        </div>

    </body>

</html>
