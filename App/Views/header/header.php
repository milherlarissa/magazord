<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Magazord</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js"></script>

    <!-- <link rel="stylesheet" type="text/css" href="../../../public/css/principal.css"> -->

    <style> 

        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        body {
            display: flex;
            flex-direction: column;
        }

        .row{
            height: 100vh;
        }

        /* MENU */
        .nav.flex-column ul.nav.flex-column {
            margin-left: 20px; 
        }

        .sidebar{
            padding-top: 1%;
        }

        .nav.sidebar {
            height: 100vh; 
            position: fixed;
            top: 0; 
            left: 0;     
        }

        .corpo-principal{
            padding: 3%;
        }

        .botoes{
            margin: 5% 10%;
        }
    </style>
</head>
