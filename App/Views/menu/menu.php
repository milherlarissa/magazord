<nav class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
    <div class="position-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="../index.php">
                    Início
                </a>
            </li>

            <li class="nav-item">
                Pessoas
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="../pessoa/listagem.php">
                            Listagem
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../pessoa/cadastrar.php">
                            Cadastrar
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                Contato
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="../contato/listagem.php">
                            Listagem
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../contato/cadastrar.php">
                            Cadastrar
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
